# INSTALL

#### CLI CHECK
```
ps -ef | grep nginx
```

#### Login with Pair Key
```
$ ssh -i cuongnq-aws-asia-pair-key.pem ec2-user@13.229.229.40
```

#### Enable ec2 password
- Change password

    ```sudo passwd ec2-user```

- Update the PasswordAuthentication paramete

    ```sudo vim /etc/ssh/sshd_config```

    PasswordAuthentication ```yes```

- Restart the SSH service

    ```sudo service sshd restart```


#### Install Web Service
```
$ sudo su
# yum install -y nginx
# service nginx start
# ps -ef | grep nginx
```

#### Install PHP and Other Service
```
# yum install -y php72 php72-fpm php72-cli php72-common php72-json php72-mbstring php72-mysqlnd php72-pdo php72-process php72-xml openssl zip unzip php72-zip git php72-gd
```
##### Config NGINX
```
# touch /etc/nginx/conf.d/vhosts.conf
# vim /etc/nginx/conf.d/vhosts.conf
```
```
server {
    listen 80;
    server_name localhost;
    root /var/www/html/public;

    index index.html index.htm index.php;

    charset utf-8;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass    unix:/var/run/php-fpm/www.sock;
        fastcgi_index   index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }
}
```

##### Edit NGINX port default in 
```
# vim /etc/nginx/nginx.conf
# service nginx restart
```

#### Install MySQL
```
# wget https://dev.mysql.com/get/mysql57-community-release-el6-11.noarch.rpm
# yum install mysql57-community-release-el6-11.noarch.rpm
# yum install mysql-community-server
```

##### Login MYSQL without password
```
# service mysqld stop
# mysqld_safe --skip-grant-tables
```

##### Create New Mysql User
```
# mysql -u root
mysql> CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'Pwd123!';
mysql> GRANT ALL ON *.* to newuser@localhost IDENTIFIED BY 'Pwd123!';
```

#### Install Composer
```
# curl -sS https://getcomposer.org/installer | sudo php
# mv composer.phar /usr/local/bin/composer
# ln -s /usr/local/bin/composer /usr/bin/composer
```

